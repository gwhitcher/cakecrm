<?php
class ClientsController extends AppController {
	
	public function clients() {
		$this->loadModel('Clients');
		$this->set('clients', $this->Clients->find('all', array('order' => 'name DESC')));
	}
	
	public function client_view($id = null) {
		//LOAD CLIENT INFO
		$this->loadModel('Clients');
		$client = $this->Clients->findById($id);
		$this->set(compact('client'));
		//LOAD INVOICE INFO
		$this->loadModel('Invoices');
		$this->set('invoices', $this->Invoices->find('all', array('conditions' => 'client_id = '.$id.'', 'order' => 'id DESC')));
	}
	
	public function client_add() {
		$this->loadModel('Clients');
		$this->set('title_for_layout', 'Add Client');
			if ($this->request->is('post')) { 
				$this->Clients->save($this->request->data);
				$this->Session->setFlash('Client added!');
				$this->redirect(array('action' => 'client_view', $this->Clients->id));
			}
		
	}
	
	public function client_edit($id = null) {
		$this->set('title_for_layout', 'Edit Client');
		$this->loadModel('Clients');
		$this->Clients->id = $id;
		$clients = $this->Clients->findById($id);
		$this->set(compact('clients'));
		if ($this->request->is('post') || $this->request->is('put')) {
				$this->Clients->save($this->request->data);
				$this->Session->setFlash('Client edited!');
				$this->redirect($this->referer());
		}
		else {
				$this->request->data = $this->Clients->read();
			}
	}
	
	public function client_delete($id = null) {
		$this->set('title_for_layout', 'Delete Client');
		$this->loadModel('Clients');
		if (!$id) {
			$this->Session->setFlash('Invalid id for client');
			$this->redirect($this->referer());
		}
		if ($this->Clients->delete($id)) {
			$this->Session->setFlash('Client deleted');
			$this->redirect($this->referer());
		}
	}
	
}
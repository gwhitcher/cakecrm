<?php
/**
 * Contact Controller
 */
App::uses('CakeEmail', 'Network/Email');
class TicketsController extends AppController {
	
	public function tickets() {
		$this->set('title_for_layout', 'Tickets');
		$this->loadModel('Tickets');
		$this->set('tickets', $this->Tickets->find('all', array('order' => 'id DESC')));
	}
	
	public function ticket_view($id = null) {
		//LOAD TICKET
		$this->loadModel('Tickets');
		$ticket = $this->Tickets->findById($id);
		$this->set(compact('ticket'));
		$this->set('title_for_layout', 'Ticket ID:'.$ticket['Tickets']['id'].'');
	}
	
	public function ticket_delete($id = null) {
		$this->set('title_for_layout', 'Delete Ticket');
		$this->loadModel('Tickets');
		if (!$id) {
			$this->Session->setFlash('Invalid id for ticket');
			$this->redirect($this->referer());
		}
		if ($this->Tickets->delete($id)) {
			$this->Session->setFlash('Ticket deleted');
			$this->redirect($this->referer());
		}
	}
	
	public function submit_ticket() {
		$this->loadModel('Tickets');
		$this->set('title_for_layout', 'Contact '.Configure::read('site_title').'');
		// form posted
		if ($this->request->is('post')) {
			if($this->data['Tickets']['captcha']!=$this->Session->read('captcha'))
			{
			$this->Session->setFlash(__('Please enter correct captcha code and try again.', true));
			}
			else {
			// create
			$this->Tickets->create();
				
			//SEND EMAIL
			$Email = new CakeEmail();
			$message = "".$this->data['Tickets']['message']."";
			$Email->from(array($this->data['Tickets']['email'] => $this->data['Tickets']['name']))
  					  ->to(Configure::read('admin_email'))
  					  ->subject('Ticket - '.$this->data['Tickets']['type'].'')
   					  ->send($message);

			// attempt to save
			if ($this->Tickets->save($this->request->data)) {
				$this->Session->setFlash('Your message has been submitted');
				$this->redirect(array('action' => 'index'));
			// form validation failed
			} else {
				// check if file has been uploaded, if so get the file path
				if (!empty($this->Contact->data['Tickets']['filepath']) && is_string($this->Tickets->data['Tickets']['filepath'])) {
					$this->request->data['Tickets']['filepath'] = $this->Tickets->data['Tickets']['filepath'];
				}
			}
			}
		}
	}
	
	/* LOAD CAPTCHA IMAGE */
	public function captcha_image(){
    App::import('Vendor', 'captcha/captcha');
    $captcha = new captcha();
    $captcha->show_captcha();
	}
	
}
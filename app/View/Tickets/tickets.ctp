<h2>Tickets</h2>
<table width="100%">
	<tr>
    	<th>ID</th>
        <th>Delete</th>
    </tr>
<?php foreach($tickets as $ticket) {
	echo '<tr>';
	echo '<td><a href="'.Configure::read('BASE_URL').'/ticket/'.$ticket['Tickets']['id'].'">'.$ticket['Tickets']['name'].' (Ticket ID#'.$ticket['Tickets']['id'].')</a></td>';
	echo '<td align="center">'.$this->Form->postLink('DELETE', array('action' => 'ticket_delete', $ticket['Tickets']['id']), array('confirm'=>'Are you sure you want to delete that ticket?')).'</td>';
	echo '</tr>';
}
?>
</table>
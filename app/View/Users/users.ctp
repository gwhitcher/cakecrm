<h2>Users</h2>
<a href="<?php echo Configure::read('BASE_URL');?>/user/add">Add User</a>
<table width="100%">
	<tr>
    	<th>ID</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
<?php foreach($users as $user) {
	echo '<tr>';
	echo '<td>'.$user['User']['username'].'</td>';
	echo '<td align="center"><a href="'.Configure::read('BASE_URL').'/user/edit/'.$user['User']['id'].'">EDIT</a>';
	echo '<td align="center">'.$this->Form->postLink('DELETE', array('action' => 'user_delete', $user['User']['id']), array('confirm'=>'Are you sure you want to delete that user?')).'</td>';
	echo '</tr>';
}
?>
</table>
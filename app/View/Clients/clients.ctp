<h2>Clients</h2>
<a href="<?php echo Configure::read('BASE_URL');?>/client/add">Add Client</a>
<table width="100%">
	<tr>
    	<th>Client</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
<?php foreach($clients as $client) {
	echo '<tr>';
	echo '<td><a href="'.Configure::read('BASE_URL').'/client/'.$client['Clients']['id'].'">'.$client['Clients']['name'].'</a></td>';
	echo '<td align="center"><a href="'.Configure::read('BASE_URL').'/client/edit/'.$client['Clients']['id'].'">EDIT</a>';
	echo '<td align="center">'.$this->Form->postLink('DELETE', array('action' => 'client_delete', $client['Clients']['id']), array('confirm'=>'Are you sure you want to delete that client?')).'</td>';
	echo '</tr>';
}
?>
</table>
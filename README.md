### CakeCRM ###

A client relations manager written in PHP in the CakePHP framework.  Written by [George Whitcher](http://georgewhitcher.com).

### INSTALLATION ###

* Setup a database and user.
* Login to PHPMYADMIN and insert the cakecrm.sql file into the database.
* Configure the database file by going to /app/config/database.php
* Configure your site specific values by going to /app/config/bootstrap.php
* Your site should now be installed.

### ADMINISTRATION ###

* The default user and password is admin.  It is suggested you setup a new user with a secure password.

### QUESTIONS/COMMENTS ###

* If you have any questions the fastest way to get results is by visiting my website [http://www.georgewhitcher.com](http://www.georgewhitcher.com)
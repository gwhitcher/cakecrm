<h2>Tickets</h2>
<div id="contactform">
<?php 
echo $this->Form->create('Tickets', array('type'=>'file'));
echo $this->Form->input('name');
echo $this->Form->input('email');
echo $this->Form->input('phone');
echo $this->Form->input('type', array(
    'options' => array(
	'urgent' => 'Urgent',
	'normal' => 'Normal',
	'other' => 'Other'
	),
    'empty' => '(choose one)'
));
echo $this->Form->input('message', array('type'=>'textarea'));
?>
<img class="captcha" id="captcha" src="<?php echo Configure::read('BASE_URL');?>/tickets/captcha_image" alt="" /> <a href="javascript:void(0);" onclick="javascript:document.getElementById('captcha').src='<?php echo Configure::read('BASE_URL');?>/contact/captcha_image?' + Math.round(Math.random(0)*1000)+1;"><img class="form_refresh" src="<?php echo Configure::read('BASE_URL');?>/img/contact/refresh.png" /></a>
<?php
echo $this->Form->input('captcha', array('label'=>'Enter captcha code'));
echo $this->Form->end('Submit');
?>
</div>
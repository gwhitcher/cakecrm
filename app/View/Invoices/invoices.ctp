<h2>Invoices</h2>
<a href="<?php echo Configure::read('BASE_URL');?>/invoice/add">Add Invoice</a>
<table width="100%">
	<tr>
    	<th>ID</th>
    	<th>Status</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
<?php foreach($invoices as $invoice) {
	echo '<tr>';
	echo '<td><a href="'.Configure::read('BASE_URL').'/invoice/'.$invoice['Invoices']['id'].'">'.$invoice['Invoices']['name'].' (Invoice ID#'.$invoice['Invoices']['id'].')</a></td>';
	if ($invoice['Invoices']['status'] == 0) {
	    $status = 'Pending';
	} else {
	    $status = 'Paid';
	}
	echo '<td align="center">'.$status.'</td>';
	echo '<td align="center"><a href="'.Configure::read('BASE_URL').'/invoice/edit/'.$invoice['Invoices']['id'].'">EDIT</a>';
	echo '<td align="center">'.$this->Form->postLink('DELETE', array('action' => 'invoice_delete', $invoice['Invoices']['id']), array('confirm'=>'Are you sure you want to delete that invoice?')).'</td>';
	echo '</tr>';
}
?>
</table>
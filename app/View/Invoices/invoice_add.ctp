<h1>Add Invoice</h1>
<?php echo $this->Form->create('Invoices', array('enctype'=>'multipart/form-data'));?>
<fieldset>
<legend>Add Invoice</legend>
<?php
echo $this->Form->input('client_id', array('id' => 'client_id', 'empty' => 'Please choose'));
echo $this->Form->input('name', array('id' => 'name'));
echo $this->Form->input('address', array('id' => 'address'));
?>
</fieldset>
<?php echo $this->Form->end('Submit');?>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
$(function() { 
 $("#client_id").bind("change", function() {
     $.ajax({
         type: "GET", 
         url: "<?php echo Configure::read('BASE_URL');?>/load_client_name",
         data: "client_id="+$("#client_id").val(),
         success: function(data) {
             $("#name").val(data);
         }
     });
	 
 });
 $("#client_id").bind("change", function() {
     $.ajax({
         type: "GET", 
         url: "<?php echo Configure::read('BASE_URL');?>/load_client_address",
         data: "client_id="+$("#client_id").val(),
         success: function(data) {
             $("#address").val(data);
         }
     });
	 
 });
 
});
</script>
<h2>Invoice ID: <?php echo $invoice['Invoices']['id']; ?></h2>
<table width="100%">
	<tr>
    <td align="left" valign="top">
	<?php echo $invoice['Invoices']['name']; ?>
    </br>
	<?php echo nl2br($invoice['Invoices']['address']); ?></td>
    <td align="right" valign="top"><?php echo Configure::read('company_info');?></td>
    </tr>
</table>
<table width="100%">
	<tr>
    <th>Description</th>
    <th>Quantity</th>
    <th>Unit Price</th>
    <th>Total Price</th>
    <th class="no_print">Edit</th>
    <th class="no_print">Delete</th>
    </tr>
<a class="no_print" href="<?php echo Configure::read('BASE_URL'); ?>/invoice/item/add?invoice_id=<?php echo $invoice['Invoices']['id']; ?>">Add Invoice Item</a>
<?php 
	$final_total_array = array();
	foreach($invoice_items as $invoice_item) {
	echo '<tr>';
	echo '<td>'.$invoice_item['Invoice_items']['description'].'</td>';
	echo '<td>'.$invoice_item['Invoice_items']['quantity'].'</td>';
	echo '<td>$'.$invoice_item['Invoice_items']['price'].'</td>';
	$invoice_item_total = $invoice_item['Invoice_items']['quantity'] * $invoice_item['Invoice_items']['price'];
	echo '<td>$'.$invoice_item_total.'</td>';
	echo '<td class="no_print"><a href="'.Configure::read('BASE_URL').'/invoice/item/edit/'.$invoice_item['Invoice_items']['id'].'">EDIT</a></td>';
	echo '<td class="no_print">'.$this->Form->postLink('DELETE', array('action' => 'invoice_item_delete', $invoice_item['Invoice_items']['id']), array('confirm'=>'Are you sure you want to delete that invoice item?')).'</td>';
	echo '</tr>';
	$final_total_array[] = $invoice_item_total;
} ?>
	<tr>
    <td colspan="4" align="right">
    <?php 
	$total = 0;
	foreach($final_total_array as $total_item) {
		$total += $total_item;
	}
	?>
    <strong>Total:</strong> $<?php echo $total;?>
    </td>
    </tr>
</table>
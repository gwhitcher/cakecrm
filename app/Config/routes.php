<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'users', 'action' => 'dashboard'));
	
	/** ADMIN MAIN & USER **/
	Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));
	Router::connect('/user/add', array('controller' => 'users', 'action' => 'user_add'));
	Router::connect('/user/edit/*', array('controller' => 'users', 'action' => 'user_edit'));
	Router::connect('/user/delete/*', array('controller' => 'users', 'action' => 'user_delete'));
	Router::connect('/users/*', array('controller' => 'users', 'action' => 'users'));
	
	/* CLIENTS */
	Router::connect('/clients', array('controller' => 'clients', 'action' => 'clients'));
	Router::connect('/client/add', array('controller' => 'clients', 'action' => 'client_add'));
	Router::connect('/client/edit/*', array('controller' => 'clients', 'action' => 'client_edit'));
	Router::connect('/client/delete/*', array('controller' => 'clients', 'action' => 'client_delete'));
	Router::connect('/client/*', array('controller' => 'clients', 'action' => 'client_view'));
	
	/* INVOICE */
	Router::connect('/invoices', array('controller' => 'invoices', 'action' => 'invoices'));
	Router::connect('/invoice/add', array('controller' => 'invoices', 'action' => 'invoice_add'));
	Router::connect('/invoice/edit/*', array('controller' => 'invoices', 'action' => 'invoice_edit'));
	Router::connect('/invoice/delete/*', array('controller' => 'invoices', 'action' => 'invoice_delete'));
	Router::connect('/invoice/item/add', array('controller' => 'invoices', 'action' => 'invoice_item_add'));
	Router::connect('/invoice/item/edit/*', array('controller' => 'invoices', 'action' => 'invoice_item_edit'));
	Router::connect('/invoice/*', array('controller' => 'invoices', 'action' => 'invoice_view'));
	
	/* TICKETS */
	Router::connect('/tickets', array('controller' => 'tickets', 'action' => 'tickets'));
	Router::connect('/ticket', array('controller' => 'tickets', 'action' => 'submit_ticket'));
	Router::connect('/tickets/captcha_image', array('controller' => 'tickets', 'action' => 'captcha_image'));
	Router::connect('/ticket/delete/*', array('controller' => 'tickets', 'action' => 'ticket_delete'));
	Router::connect('/ticket/*', array('controller' => 'tickets', 'action' => 'ticket_view'));
	
	/* AJAX QUERIES */
	Router::connect('/load_client_name', array('controller' => 'ajax', 'action' => 'load_client_name'));
	Router::connect('/load_client_address', array('controller' => 'ajax', 'action' => 'load_client_address'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';

//Add available extensions
Router::parseExtensions('html', 'rss', 'php', 'xml');
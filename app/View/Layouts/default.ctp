<!DOCTYPE html>
<html>
<?php echo $this->Html->charset(); ?>
<?php if (!empty($scripts_for_layout)) { echo $scripts_for_layout; } else { ?>
<meta name="description" content="<?php echo Configure::read('metadescription');?>" />
<meta name="keywords" content="<?php echo Configure::read('metakeywords');?>" />
<?php } ?>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo Configure::read('site_title');?> : <?php echo $this->fetch('title'); ?></title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('styles');
		echo $this->Html->css('print');
		echo $this->Html->script('jquery');
		echo $this->Html->script('default');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="container">
    
		<header id="header">
        <h1><a href="<?php echo Configure::read('BASE_URL');?>"><?php echo Configure::read('site_title');?></a></h1>
        <h2><?php echo Configure::read('metadescription');?></h2>
        </header>
        
        <nav id="nav">
        <ul>
        	<li><a href="<?php echo Configure::read('BASE_URL');?>">Home</a></li>
            <li><a href="<?php echo Configure::read('BASE_URL');?>/clients">Clients</a></li>
            <li><a href="<?php echo Configure::read('BASE_URL');?>/invoices">Invoices</a></li>
            <li><a href="<?php echo Configure::read('BASE_URL');?>/tickets">Tickets</a></li>
            <li><a href="<?php echo Configure::read('BASE_URL');?>/ticket">Submit Ticket</a></li>
            <li><a href="<?php echo Configure::read('BASE_URL');?>/users">Users</a></li>
            <li><a href="<?php echo Configure::read('BASE_URL');?>/logout">Logout</a></li>
        </ul>
        </nav>
		
        <section id="content">
        <?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
        </section>
        
		<footer id="footer">
		<p>&copy; Copyright 2014 - <?php echo date("Y"); ?> - <?php echo Configure::read('site_title');?> - All Rights Reserved.</p>
		</footer>
        
	</div>

</body>
</html>
<h1>Edit Invoice Item</h1>
<?php echo $this->Form->create('Invoice_items', array('enctype'=>'multipart/form-data'));?>
<fieldset>
<legend>Edit Invoice Item</legend>
<?php
echo $this->Form->input('description', array('type' => 'textarea'));
echo $this->Form->input('quantity');
echo $this->Form->input('price');
?>
</fieldset>
<?php echo $this->Form->end('Submit');?>
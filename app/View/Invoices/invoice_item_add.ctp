<h1>Add Invoice Item</h1>
<?php echo $this->Form->create('Invoice_items', array('enctype'=>'multipart/form-data'));?>
<fieldset>
<legend>Add Invoice Item</legend>
<?php
echo $this->Form->input('invoice_id', array('type' => 'hidden', 'default' => $_GET['invoice_id']));
echo $this->Form->input('description', array('type' => 'textarea'));
echo $this->Form->input('quantity');
echo $this->Form->input('price');
?>
</fieldset>
<?php echo $this->Form->end('Submit');?>
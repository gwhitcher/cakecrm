<?php
// app/Controller/UsersController.php
App::uses('AppController', 'Controller');

class UsersController extends AppController {
  
   /* DASHBOARD */
   public function dashboard() {
	$this->set('title_for_layout', 'Administration'); 
	//LOAD TICKETS
	$this->loadModel('Tickets');
	$this->set('tickets', $this->Tickets->find('all', array('limit' => 5, 'order' => 'id DESC')));
	//LOAD CLIENTS
	$this->loadModel('Clients');
	$this->set('clients', $this->Clients->find('all', array('limit' => 5, 'order' => 'id DESC')));
	//LOAD INVOICES
	$this->loadModel('Invoices');
	$this->set('invoices', $this->Invoices->find('all', array('limit' => 5, 'order' => 'id DESC'))); 
   }
   
    /* USERS */
	public function users() {
		$this->set('title_for_layout', 'Users');
		$this->loadModel('User');
		$this->set('users', $this->User->find('all', array('order' => 'id DESC')));
	}
	
	public function login() {
    if ($this->request->is('post')) {
        if ($this->Auth->login()) {
            return $this->redirect($this->Auth->redirect());
        }
        $this->Session->setFlash(__('Invalid username or password, try again'));
    }
	}

	public function logout() {
   		return $this->redirect($this->Auth->logout());
	}
	
    public function user_add() {
        $this->loadModel('User');
		if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The user could not be saved. Please, try again.')
            );
        }
    }

    public function user_edit($id = null) {
        $this->loadModel('User');
		$this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('post') || $this->request->is('put')) {
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The user could not be saved. Please, try again.')
            );
        } else {
            $this->request->data = $this->User->read(null, $id);
            unset($this->request->data['User']['password']);
        }
    }

    public function user_delete($id = null) {
		$this->loadModel('User');
		
        // Prior to 2.5 use
        // $this->request->onlyAllow('post');

		$this->request->allowMethod('post');

        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->User->delete()) {
            $this->Session->setFlash(__('User deleted'));
            return $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash(__('User was not deleted'));
        return $this->redirect(array('action' => 'index'));
    }

}
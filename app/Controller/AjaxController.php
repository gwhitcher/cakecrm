<?php
App::uses('AppController', 'Controller');
class AjaxController extends AppController {
	
	public $components = array('Paginator', 'RequestHandler');
    public $helpers = array('Time', 'Text', 'Js');
	
	//AJAX QUERY FOR CLIENT NAME
	public function load_client_name() {
        $this->loadModel('Clients');
		$this->layout = 'ajax';
		$id = $_GET['client_id'];
        $client = $this->Clients->findById($id);
		$this->set(compact('client'));   
    }
	
	//AJAX QUERY FOR CLIENT ADDRES
	public function load_client_address() {
        $this->loadModel('Clients');
		$this->layout = 'ajax';
		$id = $_GET['client_id'];
        $client = $this->Clients->findById($id);
		$this->set(compact('client'));   
    }
}
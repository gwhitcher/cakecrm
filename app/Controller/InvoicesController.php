<?php
App::uses('AppController', 'Controller');
class InvoicesController extends AppController {
	
	public $components = array('Paginator', 'RequestHandler');
    public $helpers = array('Time', 'Text', 'Js');
	
	public function invoices() {
		$this->set('title_for_layout', 'Invoices');
		$this->loadModel('Invoices');
		$this->set('invoices', $this->Invoices->find('all', array('order' => 'id DESC')));
	}
	
	public function invoice_view($id = null) {
		//LOAD INVOICE
		$this->loadModel('Invoices');
		$invoice = $this->Invoices->findById($id);
		$this->set(compact('invoice'));
		//LOAD CLIENT
		$this->loadModel('Clients');
		$client = $this->Clients->findById($invoice['Invoices']['client_id']);
		$this->set(compact('client'));
		//LOAD INVOICE ITEMS
		$this->loadModel('Invoice_items');
		$this->set('invoice_items', $this->Invoice_items->find('all', array('conditions' => 'invoice_id = '.$id.'', 'order' => 'id DESC')));
			
	}
	
	public function invoice_add() {
		$this->loadModel('Invoices');
		$this->set('title_for_layout', 'Add Invoice');
		
		$this->loadModel('Clients');
		$clients = $this->Clients->find('list');
		$this->set(compact('clients'));
		
			if ($this->request->is('post')) { 
				$this->Invoices->save($this->request->data);
				$this->Session->setFlash('Invoice added!');
				$this->redirect(array('action' => 'invoice_view', $this->Invoices->id));
			}
	}
	
	public function invoice_edit($id = null) {
		$this->set('title_for_layout', 'Edit Invoice');
		$this->loadModel('Invoices');
		$this->loadModel('Clients');
		$clients = $this->Clients->find('list');
		$this->set(compact('clients'));
		$this->Invoices->id = $id;
		$invoices = $this->Invoices->findById($id);
		$this->set(compact('invoices'));
		if ($this->request->is('post') || $this->request->is('put')) {
				$this->Invoices->save($this->request->data);
				$this->Session->setFlash('Invoice edited!');
				$this->redirect($this->referer());
		}
		else {
				$this->request->data = $this->Invoices->read();
			}
	}
	
	public function invoice_delete($id = null) {
		$this->set('title_for_layout', 'Delete Invoice');
		$this->loadModel('Invoices');
		if (!$id) {
			$this->Session->setFlash('Invalid id for invoice');
			$this->redirect($this->referer());
		}
		if ($this->Invoices->delete($id)) {
			$this->Session->setFlash('Invoice deleted');
			$this->redirect($this->referer());
		}
	}
	
	//INVOICE ITEMS
	public function invoice_item_add() {
		$this->loadModel('Invoice_items');
		$this->set('title_for_layout', 'Add Invoice');
			if ($this->request->is('post')) { 
				$this->Invoice_items->save($this->request->data);
				$this->Session->setFlash('Invoice item added!');
				$invoice_id = $this->request->data['Invoice_items']['invoice_id'];
				$this->redirect(array('action' => 'invoice_view', $invoice_id));
			}
	}
	
	public function invoice_item_edit($id = null) {
		$this->set('title_for_layout', 'Edit Invoice Item');
		$this->loadModel('Invoice_items');
		$this->Invoice_items->id = $id;
		$invoice_items = $this->Invoice_items->findById($id);
		$this->set(compact('invoice_items'));
		if ($this->request->is('post') || $this->request->is('put')) {
				$this->Invoice_items->save($this->request->data);
				$this->Session->setFlash('Invoice item edited!');
				$this->redirect($this->referer());
		}
		else {
				$this->request->data = $this->Invoice_items->read();
			}
	}
	
	public function invoice_item_delete($id = null) {
		$this->set('title_for_layout', 'Delete Invoice Item');
		$this->loadModel('Invoice_items');
		if (!$id) {
			$this->Session->setFlash('Invalid id for invoice item');
			$this->redirect($this->referer());
		}
		if ($this->Invoice_items->delete($id)) {
			$this->Session->setFlash('Invoice item deleted');
			$this->redirect($this->referer());
		}
	}
}
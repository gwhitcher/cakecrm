<?php echo $this->Form->create('User');?>
<fieldset>
<legend>Edit User</legend>
<?php
echo $this->Form->input('username');
echo $this->Form->input('password');
echo $this->Form->input('role', array(
    'options' => array('regular' => 'regular', 'admin' => 'admin'),
    'empty' => '(choose one)'
));
?>
<?php echo $this->Form->end('Submit');?>
</fieldset>
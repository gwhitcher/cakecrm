<h2>Dashboard</h2>
<h3>Recent Tickets</h3>
<ul>
<?php foreach($tickets as $ticket) {
	echo '<li><a href="'.Configure::read('BASE_URL').'/ticket/'.$ticket['Tickets']['id'].'">'.$ticket['Tickets']['name'].' (Ticket ID:'.$ticket['Tickets']['id'].')</a></li>';
}
?>
</ul>
<h3>Recent Clients</h3>
<ul>
<?php foreach($clients as $client) {
	echo '<li><a href="'.Configure::read('BASE_URL').'/client/'.$client['Clients']['id'].'">'.$client['Clients']['name'].'</a></li>';
}
?>
</ul>
<h3>Recent Invoices</h3>
<ul>
<?php foreach($invoices as $invoice) {
	echo '<li><a href="'.Configure::read('BASE_URL').'/invoice/'.$invoice['Invoices']['id'].'">'.$invoice['Invoices']['name'].' (Invoice ID:'.$invoice['Invoices']['id'].')</a></li>';
}
?>
</ul>